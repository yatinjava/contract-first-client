package com.contractfirst.client.controller;

import com.contractfirst.client.api.AccountApi;
import com.contractfirst.client.model.InlineObject;
import com.contractfirst.client.model.InlineResponse200;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class Controller {

    private final AccountApi accountAPI;

    public Controller(AccountApi accountApi) {
        this.accountAPI = accountApi;
        this.accountAPI.getApiClient()
                .setBasePath("http://localhost:8080");
    }

    @GetMapping("/call")
    public InlineResponse200 callClient() {
        InlineObject request = new InlineObject();
        request.accountType("Savings")
                .residencyStatus("Resident")
                .name("Asif");

        log.info("Sending generated request: {}", request);
        InlineResponse200 account = this.accountAPI.createAccount(request);
        log.info("Received response: {}", account);
        return account;
    }
}
